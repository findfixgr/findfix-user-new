import React, { Component } from 'react';
// import { Helmet } from 'react-helmet';

import Header from './Containers/Header/Header';
import Layout from './Containers/Layout/Layout';
import Footer from './Containers/Footer/Footer';

import './App.css';

class App extends Component {
  render() {
    return (

        <div>
          {/* <Helmet>
            <title>Turbo Todo</title>
            <meta name="description" content="Todos on steroid!" />
            <meta name="theme-color" content="#008f68" />
          </Helmet> */}
          <Header {...this.props} />
          <Layout {...this.props} />
          <Footer {...this.props}/>
        </div>
    );
  }
}

export default App;
