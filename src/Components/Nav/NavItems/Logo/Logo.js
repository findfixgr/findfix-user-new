import React from 'react'
import './Logo.css';

const logo = (props) => {
    return (
        <a className="navbar-brand" href="/"><img src="/img/logo.svg" alt="FindFix" width="150px" /></a>
    );
}

export default logo;