import React from 'react';
import { Switch , Route } from 'react-router-dom';

import ModalFilters from '../../Containers/Filters/ModalFilters';
import UserNameAndAddress from '../ModalEnquiry/UserNameAndAddress';
import Response from '../ModalEnquiry/Response';
// import Filters from '../../Containers/Filters/Filters';



const enquiry = (props) => {
//   console.log(props)
    return (
        
        <div>
             <Switch>
                <Route path="/shops/response"  component ={ Response } />
                <Route path="/singleshop/:shopId"  component ={ ModalFilters } />
                <Route path="/shops/address"  component ={ UserNameAndAddress } />
                <Route path="/shops" exact component ={ ModalFilters } />    
            </Switch>
        </div>
    );
}

export default enquiry;