import React from 'react';
import ContentLoader from "react-content-loader";

const myContentLoader = props => (
	<ContentLoader
		height={270}
		width={400}
		speed={2}
		primaryColor="#f3f3f3"
		secondaryColor="#ecebeb"
		{...props}
	>
		<rect x="182" y="21.27" rx="0" ry="0" width="8.5" height="0" /> 
		<rect x="303.5" y="79.27" rx="0" ry="0" width="0" height="0" /> 
		<rect x="18" y="12.27" rx="0" ry="0" width="137" height="97" /> 
		<rect x="189" y="21.27" rx="0" ry="0" width="161" height="33" /> 
		<rect x="190" y="71.27" rx="0" ry="0" width="161" height="33" /> 
		<rect x="22" y="214.27" rx="0" ry="0" width="353.5" height="45.44" /> 
		<rect x="113" y="157.27" rx="0" ry="0" width="0" height="0" /> 
		<rect x="30" y="148.27" rx="0" ry="0" width="2" height="2" /> 
		<rect x="26.24" y="135.27" rx="0" ry="0" width="342.72" height="12.24" /> 
		<rect x="25.24" y="162.27" rx="0" ry="0" width="342.72" height="12.24" /> 
		<rect x="26.23" y="189.47" rx="0" ry="0" width="342.72" height="12.24" />
	</ContentLoader>
)

export default myContentLoader