import React, { Component }from 'react';
import {withRouter} from 'react-router-dom';
import { connect } from 'react-redux';
import Filters from '../../Containers/Filters/Filters';
import './Topbar.css';


class Topbar extends Component {
render() {
    return (
        <div className="topbar">
            <h2>Αποτελέσματα κοντά στην περιοχή {this.props.match.params.locality}</h2>
	        <p>Ανακάλυψε τεχνικούς στην περιοχή σου και μάθε περισσότερα για αυτούς! Δες κριτικές και στείλε αίτημα επισκευής ή ζητήσετε άμεση επισκευής σε online τεχνικούς.</p>
            <p>Χρησιμοποίησε τα φίλτρα και περιόρισε τα αποτελέσματα </p>
            <Filters />
        </div>
    );
}

}

const mapStateToProps = state => {

    return {
        noOfShops : state.shopsReducer.numberOfShops,
    };
};

export default connect(mapStateToProps, null)(withRouter(Topbar));