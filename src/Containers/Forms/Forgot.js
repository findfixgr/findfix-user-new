import React, { Component } from 'react';
import axios from '../../axios';
import Loader from '../../Components/UI/Loader/Loader';

class Forgot extends Component {

    state= {
        user_email : '',
        loading: false,
        responseMessage : false,
        message : null,
    }

    handleChange = (event) => {
        this.setState({user_email: event.target.value});
      }


    submitHandler = ( event ) => {
        event.preventDefault();
        this.setState( { loading: true } );

        let data = {
            email : this.state.user_email
        }
        axios.post( '/user/forgot', data )
            .then( response => {
                this.setState( { loading: false, responseMessage : true,
                    message : response.data.message});
            } )
            .catch( error => {
                this.setState( { loading: false, responseMessage : true,
                    message : error.response.data.message});
            } );
    }
    
    render () {
        let loader = null;
        if (this.state.loading) { loader = <Loader />};

        let message = null;
        if (this.state.responseMessage) 
            { message = (<div className="alert alert-dark" role="alert">{this.state.message}</div>);
            setTimeout(() => {
                this.setState({responseMessage : false});
              }, 6000);
      }
    return (
        <div>
            { loader }
            <div className="container forgot">
                <h4 className="page-title">Υπενθύμιση Κωδικού</h4>
                <p>Συμπλήρωσε το email σου, και αν είσαι χρήστης του FindFix θα σου σταλεί ένα email με οδηγίες για να αλλάξεις τον κωδικό σου.<br />
                Η διαδικασία είναι απλή και σύντομη</p>
                { message } {/*εμφανιζει εδω το μηνυμα απο τον server */}
                <form onSubmit={this.submitHandler}>
                    <div className="form-group">
                        <input  type="email" 
                                className="form-control" 
                                id="user_email" 
                                name="user_email" 
                                placeholder="Email" 
                                value={this.state.user_email} 
                                onChange={this.handleChange}
                                required />
                    </div>
                    <button type="submit" className="btn btn-findfix">Αποστολή</button>
                </form>
            </div>
        </div>
    );
    }
}

  export default Forgot;