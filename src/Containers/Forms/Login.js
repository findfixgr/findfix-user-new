import React, { Component, Fragment } from 'react';
import Loader from '../../Componets/UI/Loader/Loader';
import Input from '../../Componets/UI/input/input';
import Button from '../../Componets/UI/Button/Button';
import axios from '../../axios-api';
import { Cookies } from 'react-cookie';
import { Link , Redirect } from 'react-router-dom';

const cookies = new Cookies();

class Login extends Component {


state = {
    partner : {
        email: {elementType : 'input',
                elementConfig : {
                    type : 'email',
                    placeholder : 'Email'
                },
                value : ''
            },
        password: {elementType : 'input',
        elementConfig : {
            type : 'password',
            placeholder : 'Κωδικός'
        },
        value : ''
    }

    },
    loading: false,
    errorStatus : false,
    errorMessage : null,
    redirect : false,
}

orderHandler = ( event ) => {
    event.preventDefault();
    this.setState( { loading: true } );
    const formData = {};
    for (let formElementIdentifier in this.state.partner) {
        formData[formElementIdentifier] = this.state.partner[formElementIdentifier].value;
    }
    const signup = {
        email: formData.email,
        password: formData.password,
    }
    axios.post( '/user/login', signup )
        .then( response => {
            
            cookies.set('userId', response.data.userId, {maxAge:  24 * 60 * 60 * 1000}); 
            cookies.set('token', response.data.token, {maxAge:  24 * 60 * 60 * 1000}); 
            this.setState( { loading: false, redirect : true } );
            })
        .catch( error => {
            this.setState( { loading: false, errorStatus : true,
                errorMessage : error.response.data.message});
        } );
}


inputChangedHandler = (event, inputIdentifier) => {
    const updatedOrderForm = {
        ...this.state.partner
    };
    const updatedFormElement = { 
        ...updatedOrderForm[inputIdentifier]
    };
    updatedFormElement.value = event.target.value;
    updatedOrderForm[inputIdentifier] = updatedFormElement;
    this.setState({partner: updatedOrderForm});

}

render () {
  
    let error = null;
    if(this.state.errorStatus) {
        error = <div className="alert alert-warning" role="alert" >{this.state.errorMessage}</div>
        setTimeout(() => {
          this.setState({errorStatus : false});
        }, 2000);
     }

    const formElementsArray = [];
    for (let key in this.state.partner) {
        formElementsArray.push({
            id: key,
            config: this.state.partner[key]
        });
    }
    let form = (
        <form onSubmit={this.orderHandler}>
            {formElementsArray.map(formElement => (
                <Input 
                    key={formElement.id}
                    elementType={formElement.config.elementType}
                    elementConfig={formElement.config.elementConfig}
                    value={formElement.config.value}
                    changed={(event) => this.inputChangedHandler(event, formElement.id)} />
            ))}
               <Button btnType="Success">Σύνδεση</Button>

        </form>
    );
    if ( this.state.loading ) {
        form = <Loader />;
    }
    
    if (this.state.redirect) {
        if (sessionStorage.getItem('shops')){
            form = <Redirect to = '/shops' />
        } else {
         form = <Redirect to='/' />
        }
    }
    return (
        <Fragment>
            <div className="container">
                <div className="col-lg-6 offset-lg-3 col-md-6 offset-md-3 col-s-12 col-xs-12">
                    <div className="forms">
                    <h4>Σύνδεση</h4>
                    {error}
                    {form}
                    <Link to="/forgot"><p>Ξέχασες τον κωδικό σου;</p></Link>
                    <hr />
                    <p>Νέος χρήστης; <Link to="/signup">Κάνε εγγραφή.</Link></p>
                    </div>
                </div>      
            </div>
        </Fragment>
    );
}
}

  export default Login;