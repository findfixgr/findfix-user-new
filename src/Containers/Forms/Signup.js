import React, { Component, Fragment } from 'react';
import axios from '../../axios-api';
import { Cookies } from 'react-cookie';
import Loader from '../../Componets/UI/Loader/Loader';
import Confirm from '../../Componets/UI/Confirm/Confirm';
const cookies = new Cookies();

const google = window.google;
var autocomplete;

class Signup extends Component {


state = {
   
    email : '',
    password : '',
    first_name : '',
    last_name : '',
    mobile :'',
    phone : '',
    locality : '',
    street_number : '',
    route : '',
    postal_code : '',
    loading: false,
    errorStatus : false,
    errorMessage : null,
    terms : false,
    newsletter  : false
}


 geolocate() {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(function(position) {
        var geolocation = {
          lat: position.coords.latitude,
          lng: position.coords.longitude
        };
        var circle = new google.maps.Circle({
          center: geolocation,
          radius: position.coords.accuracy
        });
        autocomplete.setBounds(circle.getBounds());
      });
    }
  }


autoCompleteHandler = () => {
    var placeSearch;
    var componentForm = {
      street_number: 'short_name',
      route: 'long_name',
      locality: 'long_name',
      postal_code: 'short_name'
    };

    autocomplete = new google.maps.places.Autocomplete(
          /** @type {!HTMLInputElement} */(document.getElementById('autocomplete')),
          {types: ['geocode']});

      // When the user selects an address from the dropdown, populate the address
      // fields in the form.
 autocomplete.addListener('place_changed', fillInAddress);


    function fillInAddress() {
      // Get the place details from the autocomplete object.
      var place = autocomplete.getPlace();

      for (var component in componentForm) {
        document.getElementById(component).value = '';
        document.getElementById(component).disabled = false;
      }

      // Get each component of the address from the place details
      // and fill the corresponding field on the form.
      for (var i = 0; i < place.address_components.length; i++) {
        var addressType = place.address_components[i].types[0];
        if (componentForm[addressType]) {
          var val = place.address_components[i][componentForm[addressType]];
          document.getElementById(addressType).value = val;
        }
      }
    }

    // Bias the autocomplete object to the user's geographical location,
    // as supplied by the browser's 'navigator.geolocation' object.

}
submitHandler = ( event ) => {
    event.preventDefault();
    this.setState( { loading: true } );

    const signup = {
        email: this.state.email,
        password :this.state.password,
        first_name : this.state.first_name,
        last_name : this.state.last_name,
        mobile :this.state.mobile,
        phone : this.state.phone,
        locality : this.state.locality,
        street_number : this.state.street_number,
        route : this.state.route,
        postal_code : this.state.postal_code,
        terms : this.state.terms,
        newsletter : this.state.newsletter,
        email_confirm : false,
    }
    axios.post( '/user/signup', signup )
        .then( response => {
            this.setState( { loading: false, email_confirm :true } );
            cookies.set('userId', response.data.id); 
            
        } )
        .catch( error => {
            this.setState( { loading: false, errorStatus : true,
                errorMessage : error.response.data.message});
        } );
}


handleInputChange = (event) => {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;

    this.setState({
      [name]: value
    });
  }


render () {
    let error = null;
    if(this.state.errorStatus) {
        error = <div className="alert alert-warning" role="alert" >{this.state.errorMessage}</div>
        setTimeout(() => {
          this.setState({errorStatus : false});
        }, 2000);
     }

    let form = (
        <form  onSubmit={this.submitHandler}>
            
              <div className="form-group">
                <input type="email" className="form-control" name="email"  value={this.state.email}
            onChange={this.handleInputChange} placeholder="Email"/>
              </div>
              <div className="form-group">
                <input type="password" className="form-control" name="password" placeholder="Κωδικός"  value={this.state.password}
            onChange={this.handleInputChange} />
              </div>
            
            <div className="form-row">
                <div className="form-group col-md-6">
                    <input type="text" className="form-control" name="first_name" placeholder="Όνομα"  value={this.state.first_name}
            onChange={this.handleInputChange} required />
                </div>
                <div className="form-group col-md-6">
                    <input type="text" className="form-control" name="last_name" placeholder="Επώνυμο" required  value={this.state.last_name}
            onChange={this.handleInputChange} />
                </div>
            </div>

            <div className="form-row">
                <div className="form-group col-md-6">
                    <input type="number" className="form-control" name="mobile" placeholder="Κινητό τηλέφωνο"  value={this.state.mobile}
            onChange={this.handleInputChange} required />
                </div>
                <div className="form-group col-md-6">
                    <input type="number" className="form-control" name="phone" placeholder="Σταθερό τηλέφωνο"  value={this.state.phone}
            onChange={this.handleInputChange}/>
                </div>
            </div> 
            <div className="form-group">
                <input id="autocomplete"className="form-control" placeholder="Διεύθυνση" onFocus="geolocate()" type="text" />
            </div>
            <div className="address">
                <div className="form-row">
                    <div className="form-group col-md-6">
                        <input type="text" className="form-control" name="route"  placeholder="Οδός" id="route"  value={this.state.route}
                onChange={this.handleInputChange} />
                    </div>
                    <div className="form-group col-md-6">
                        <input type="text" className="form-control" name="street_number"  id="street_number" placeholder="Αριθμός"  value={this.state.street_number}
                onChange={this.handleInputChange} />
                    </div>
                </div> 

                <div className="form-row">
                <div className="form-group col-md-6">
                    <input type="text" className="form-control" name="locality" id="locality" placeholder="Πόλη"  value={this.state.locality}
                onChange={this.handleInputChange} />
                </div>
                <div className="form-group col-md-6">
                    <input type="text" className="form-control" name="postal_code" id="postal_code" placeholder="T.K."  value={this.state.postal_code}
                onChange={this.handleInputChange} />
                </div>
                </div>
            </div>
            <div className="form-group">
              <div className="form-check">
                <input className="form-check-input" type="checkbox" id="gridCheck1" name="terms" checked={this.state.terms}
            onChange={this.handleInputChange} required/>
                <label className="form-check-label" htmlFor="gridCheck1">
                  Με την εγγραφή αποδέχομαι τους όρους χρήσης της υπηρεσίας
                </label>
              </div>
              <div className="form-check">
                <input className="form-check-input" type="checkbox" id="gridCheck2" name="newsletter"       checked={this.state.newsletter}
            onChange={this.handleInputChange} />
                <label className="form-check-label" htmlFor="gridCheck2">
                    Επιθυμώ να λαμβάνω νέα και προσφορές.
                </label>
              </div>
            </div>
            <button type="submit" className="btn btn-primary btn-block">Εγγραφή</button>
          </form>);
          
        if (this.state.loading) { form = <Loader />};
        if (this.state.email_confirm) {
            form = <Confirm />
        }

    return (
        <Fragment>
            <div className="container">
                <div className="col-lg-6 offset-lg-3 col-md-6 offset-md-3 col-s-12 col-xs-12">
                    <div className="forms">
                        <h4>Κάνε Εγγραφή</h4>
                        {error}
                        {form}
                    </div>
                </div>
            </div>
        </Fragment>
    );
}
}

  export default Signup;