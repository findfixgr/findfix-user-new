import React, { Component } from 'react';
import axios from '../../axios';
import Loader from '../../Components/UI/Loader/Loader';

class Reset extends Component {

    state= {
        newPassword : '',
        verifyPassword: '',
        loading: false,
        responseMessage : false,
        message : null,
    }

    handleChange = (event) => {
        const target = event.target;
        const value = target.value;
        const name = target.name;
    
        this.setState({
          [name]: value
        });
      }


    submitHandler = ( event ) => {
        event.preventDefault();
        this.setState( { loading: true } );
        
        const reset = {
            verifyPassword: this.state.verifyPassword,
            newPassword : this.state.newPassword
        } 

        const { match: { params } } = this.props;

        axios.post( `/user/reset/${params.token}`, reset )
        .then( response => {
            this.setState( { loading: false } );
            this.props.history.replace('/')
        } )
        .catch( error => {
            this.setState( { loading: false, responseMessage : true,
                message : error.response.data.message});
        } );    }

    render () {
        let loader = null;
        if (this.state.loading) { loader = <Loader />};

        let message = null;
        if (this.state.responseMessage) 
            { message = (<div className="alert alert-dark" role="alert">{this.state.message}</div>);
            setTimeout(() => {
                this.setState({responseMessage : false});
              }, 6000);
      }
    return (
        <div>
            { loader }
            <div className="container forgot">
                <h4 className="page-title">Αλλαγή κωδικού</h4>
                { message } {/*εμφανιζει εδω το μηνυμα απο τον server */}
                <form onSubmit={this.submitHandler}>
                    <div className="form-group">
                        <label htmlFor="newPassword">Νέος κωδικός</label>
                        <input  type="password" 
                                className="form-control" 
                                id="newPassword" 
                                placeholder="Κωδικός"
                                name="newPassword"  
                                value={this.state.newPassword} 
                                onChange={this.handleChange}
                                required />
                    </div>
                    <div className="form-group">
                        <label htmlFor="verifyPassword">Νέος κωδικός</label>
                        <input  type="password" 
                                className="form-control" 
                                id="verifyPassword" 
                                placeholder="Κωδικός"
                                name="verifyPassword"  
                                value={this.state.verifyPassword} 
                                onChange={this.handleChange}
                                required />
                    </div>
                    <button type="submit" className="btn btn-findfix">Αποθήκευση</button>
                </form>
            </div>
        </div>
    );
    }
}

  export default Reset;