import React , { Component } from 'react';
import axios from '../../axios';

class Email extends Component {

    componentDidMount () {
        // console.log(this.props);
        var url_string = window.location.href;
        var url = new URL(url_string);
        var params = url.searchParams.get("token");
        // console.log(params);
        axios.patch( '/user/confirmemail/' + params )
        .then( response => {
        //    console.log(response);
           this.props.history.push('/');
            
        } )
        .catch( error => {
        //  console.log(error);
        } );
        // console.log(year);
    }

    
    render () {

        return (
            <p style={{textAlign : "center", minHeight : "80vh"}}>Το Email σας επιβεβαιώθηκε. Περιμένετε μέχρι να ανανεωθεί η σελίδα</p>
        )
    }
}

export default Email;