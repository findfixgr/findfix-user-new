import React, { Component } from 'react';
import axios from '../../axios';
import styles from './Favs.module.css';
import Gears from '../../Components/UI/Loader/Gears';

class Favs extends Component {
    
    state = {
        loading : true,
        favs : []
    }

    componentDidMount () {
        axios.get(`/user/faved/${localStorage.getItem('userId')}`)
        .then(favs => {
            // console.log(favs);
            this.setState({
                favs : favs.data.result,
                loading : false
            })

        })
        .catch(error => {
            console.log(error);
        })
    }

    onClick =(event) =>{
        this.props.history.push(`/shops/${event.target.name}`);
    }
    render () {
        // console.log(this.state.favs)
        let elememt = <Gears />
        if (!this.state.loading) {
            elememt = Object.values(this.state.favs).map((favs) => {
                return (
                    <div className="col-12" key={favs._id}>
                    <button type="button" className="btn btn-link btn-block" name={`${favs.locality}/${favs.shop_name}`} onClick={this.onClick}><strong>{favs.shop_name}</strong> {favs.route} {favs.street_number} {favs.locality} {favs.postal_code}  <strong>Τηλεφωνο :</strong>{favs.phone_number}</button>
                    </div>
                );
            });
        }

        return (
            <div className={styles.main}>
                <div className="container">{elememt}</div>            
            </div>
        );
    }
}

export default Favs