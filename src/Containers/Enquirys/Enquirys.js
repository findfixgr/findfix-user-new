import React, { Component } from 'react';
import axios from '../../axios';
import styles from './Enquirys.module.css';
// import Loader from '../../Components/UI/Loader/Gears';


class Enquirys extends Component {

    state = {
        enquirys : []
    }
    componentDidMount () {
        // console.log('component did');
        axios.get(`/user/enquirys/${localStorage.getItem('userId')}`)
        .then(response => {
            console.log(response.data);
            this.setState({enquirys : response.data});
        })
        .catch(error => {
            console.error(error);
        });

    }


    render () {
        // console.log(this.state.enquirys.result);
        let enquiry=null;
        let header = null;
        let data = this.state.enquirys.result;
        if (data) {
            // header= <p>{data.user.first_name} {data.user.last_name}</p>
            // console.log(data);
            enquiry = Object.values(data.reverse()).map((issue, id) => {
                let closed = null;
                let shopName = null;
                if (issue.shop) {
                    shopName = ( 
                        <div>
                            <p><strong>{issue.shop.shop_name}</strong></p> 
                            <p>{issue.shop.route} {issue.shop.street_number}</p> 
                            <p>{issue.shop.locality} , Τ.Κ. {issue.shop.postal_code}</p>
                            <p>Τηλέφωνο : {issue.shop.phone_number}</p>
                        </div> )
                }
                if (issue.closed) { closed = (<div>
                                                <p>Ολοκληρωμένη επισκευή</p>
                                                <p>Ημερομηνία ολοκλήρωσης : {issue.date_closed.substring(8,10)}/{issue.date_closed.substring(5,7)}/{issue.date_closed.substring(0,4)}</p>
                                                </div>)
                                    } 
                    else if (issue.status < 6) {
                        switch (issue.status) {
                            case 1:
                                closed = <p>Παραλαβή</p>
                                break;
                            case 2:
                                closed = <p>Έλεγχος</p>
                                break;
                            case 3:
                                closed = <p>Αναμονή ανταλλακτικού</p>
                                break;
                            case 4:
                                closed = <p>Επισκευή</p>
                                break;
                            case 5:
                                closed = <p>Ολοκλήρωση</p>
                                break;
                            default:
                                break;
                        }
                    }
                    return(
                      <tr key={issue._id} > 
                        <th scope="row">{issue.number}</th>
                        <td>
                            <p>{issue.created_at.substring(8,10)}/{issue.created_at.substring(5,7)}/{issue.created_at.substring(0,4)}</p>
                            <p>{issue.user_device.category}</p> 
                            <p>{issue.user_device.brand}</p> 
                            <p>{issue.user_device.model}</p> 
                            <p>{issue.issue}</p>
                            <p>{issue.comment}</p>
                        </td>
                        <td>
                            { shopName }
                        </td>
                        <td>
                            {closed}
                        </td>
                    </tr>
                    )})


        }
        return (
            <div className={styles.main}>
                <div className="container">
                <h1>Τα αιτήματα σας</h1>
                    {/* {JSON.stringify(this.state.enquirys)} */}
                    {header}

              <div className="table-responsive">
                 <table className="table  table-striped">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Αίτημα</th>
                            <th scope="col">Τεχνικός</th>
                            <th scope="col">Κατάσταση</th>
                        </tr>
                    </thead>
                    <tbody>
                    { enquiry }

                    </tbody>
                    </table>
            </div>
                </div>
            </div>
        );
    }
}

export default Enquirys;
    