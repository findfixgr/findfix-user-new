import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter , Link } from 'react-router-dom';
import {user, address,  modalBackdrop, issueEmailConfirmation , modalShow, modalShowUser} from '../../Store/actions';
import axios from '../../axios';
import Gears from '../../Components/UI/Loader/Gears'; 

const io = require("socket.io-client"),

// ioClient = io.connect("http://localhost:8000");
// ioClient = io.connect("https://findfix-socket.herokuapp.com/" );
ioClient = io.connect("https://socket.findfix.gr:8000/",{ transports: [ 'websocket' ]});




class UserDataForLive  extends Component {

    state = {
        shopId : null,
        name : '',
        route : '',
        street_number : '',
        locality : '',
        postal_code : '',
        phone : '',
        email : '',
        terms : false,
        newsletter  : false,
        data : false,
        error : false,
        loading : false
    }

    
componentDidMount() {
 

    if (this.props.usrId || localStorage.getItem('userId')) {
        axios.get(`/user/${localStorage.getItem('userId')}`)
        .then(response => {
            console.log(response);
            this.setState({
                name : response.data.first_name+' '+response.data.last_name,
                phone : response.data.phone,
                email : response.data.email
            });
              //get address
                axios.get(`/user/address/${localStorage.getItem('userId')}`)
                .then(response => {
                    this.setState({
                        route : response.data.route,
                        street_number : response.data.street_number,
                        postal_code : response.data.postal_code,
                        locality : response.data.locality,
                        data : true
                    });
                    axios.get(`/shop/${this.props.shpId}`)
                    .then(shop => {
                        this.setState({partner : shop.data.partner._id})
                    })
                    .catch(error => {
                        console.log(error);
                    });
                })
                .catch(error => {
                    console.log(error);
                });
        })
        .catch(error => {
            // console.log(error);
        });
    
    } else {

    axios.get(`/shop/${this.props.shpId}`)
    .then(shop => {
        this.setState({partner : shop.data.partner._id})
    })
    .catch(error => {
        console.log(error);
    });
}

ioClient.on(sessionStorage.getItem('userId'), data => {
    console.log('data from socket', data);
    this.setState({message : data.message, shopId: data.partner});
    axios.get(`/shop/${this.state.shopId}`)
    .then(response => {
        // console.log('/search/partner/',response);
        this.setState ({shop : response.data});

    })
    .catch( (error) => {
        // console.log(error);
      });

    // console.log(data);
    this.setState({waiting : false});

    // console.log(this.state.message)
    if (this.state.message === 'Το αίτημα απορρίφθηκε') {
        this.props.mHide();
    }
    
    else{
        this.props.mHide();
        this.props.emailConf();
        this.props.history.replace('/response');
    }
 }); 
 
}
    

    
handleInputChange = (event) => {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;

    this.setState({
        [name]: value
    });
    }

    submitHundler = (event) => {
        event.preventDefault();
        this.setState({loading : true});
        event.preventDefault();
        if (this.props.usrId || localStorage.getItem('userId')) {
            sessionStorage.setItem('userId', localStorage.getItem('userId') )
            let nm=this.state.name;
            let newNm = nm.split(' ')
        const userDataUpdate = {
            first_name : newNm[0],
            last_name :newNm[1],
            phone : this.state.phone
        }
        //η διευθυνση 
    
        // console.log(userDataUpdate)
        const userAddress = {
            route : this.state.route,
            locality : this.state.locality,
            street_number : this.state.street_number,
            postal_code : this.state.postal_code
        }
    
        this.props.userAddrData(userAddress);
        this.props.userData(userDataUpdate);
        
        let data = {
            category : this.props.ctgrFilter ,
            brand : this.props.brndFilter ,
            model : this.props.mdlFilter,
            issue: this.props.issFilter ,
            user : localStorage.getItem('userId'),
            shop : this.props.shpId,
            comment : this.props.cmmntFilter,
            partner : this.props.shpId,
            enquiry : true ,
        }
    
        ioClient.emit('messageFromUser', data);
        ioClient.on(sessionStorage.getItem('userId'), data => {
            console.log('data from socket', data);
            this.setState({message : data.message, shopId: data.partner});
            axios.get(`/shop/${this.state.shopId}`)
            .then(response => {
                // console.log('/search/partner/',response);
                this.setState ({shop : response.data});
    
            })
            .catch( (error) => {
                // console.log(error);
              });
    
            // console.log(data);
            this.setState({waiting : false});
    
            // console.log(this.state.message)
            if (this.state.message === 'Το αίτημα απορρίφθηκε') {
                this.props.mHide();
            }
            
            else{
                this.props.mHide();
                this.props.emailConf();
                this.props.history.replace('/response');
            }
         }); 
        }  else {
        //ο χρήστης
        const user = {
            name  : this.state.name,
            phone : this.state.phone,
            email : this.state.email,
            terms : this.state.terms,
            newsletter :this.state.newsletter
        }
        //η διευθυνση 
    
        // console.log(userDataUpdate)
        const address = {
            route : this.state.route,
            locality : this.state.locality,
            street_number : this.state.street_number,
            postal_code : this.state.postal_code
        }
    
        let userId = null;
        console.log({user})
        axios.post(`/user2/` , user)
        .then(userResponse => {
            userId =userResponse.data._id;
            sessionStorage.setItem('userId', userResponse.data._id )
            sessionStorage.setItem('password', userResponse.data.password )
            axios.post(`/user/address/${userId}`, address)
            .then(userAddressResponse => {
                let data = {
                    category : this.props.ctgrFilter ,
                    brand : this.props.brndFilter ,
                    model : this.props.mdlFilter,
                    issue: this.props.issFilter ,
                    user : userResponse.data._id,
                    shop : this.props.shpId,
                    comment : this.props.cmmntFilter,
                    partner : this.props.shpId,
                    enquiry : true ,
                }
                        ioClient.emit('messageFromUser', data);
                        ioClient.on(userId, data => {
                            console.log('data from socket', data);
                            this.setState({message : data.message, shopId: data.partner});
                            axios.get(`/shop/${this.state.shopId}`)
                            .then(response => {
                                // console.log('/search/partner/',response);
                                this.setState ({shop : response.data});
                    
                            })
                            .catch( (error) => {
                                // console.log(error);
                              });
                    
                            // console.log(data);
                            this.setState({waiting : false});
                    
                            // console.log(this.state.message)
                            if (this.state.message === 'Το αίτημα απορρίφθηκε') {
                                this.props.mHide();
                            }
                            
                            else{
                                this.props.mHide();
                                this.props.emailConf();
                                this.props.history.replace('/response');
                            }
                         }); 
                         
                    })
            .catch(error => {
                console.log(error);
            })
        })
        .catch(error => {
            this.setState({loading : false});
            this.setState({error : true})
        })
        } 
      
      }  
    
    render() {
    // console.log(this.props)

        let errorMessage = null;
        if (this.state.error) {
        errorMessage =(<div className="alert alert-warning" role="alert" >
                        Έχετε κάνει ήδη εγγραφή με αυτό το email, δοκιμάστε να κάνετε <span onClick={()=>{this.props.mShw('ΣΥΝΔΕΣΗ')}} style={{fontWeight : 'bold'}}>σύνδεση</span></div>);
        setTimeout(() => {errorMessage = (<div></div>)}, 600);
        }


        let element = ( 
        
        <div>
            { errorMessage }
            <h2>Στοιχεία επικοινωνίας</h2>
            <div className="divider"></div>
            <form  onSubmit={this.submitHundler}>
            <div className="form-group">
                <input type='email' className='form-control' id='email' name='email' placeholder='Email' 
                    value={this.state.email} onChange={this.handleInputChange}  required/>
            </div>
            <div className="form-group">
                <input type='text' className='form-control' id='name' name='name' placeholder='Όνοματεπώνυμο' 
                    value={this.state.name} onChange={this.handleInputChange} required />
            </div>
            <div className="form-row">
                <div className="form-group col-6">
                    <input type='text' className='form-control' id='route' name='route' placeholder='Οδός' 
                        value={this.state.route} onChange={this.handleInputChange} />
                </div>
                <div className="form-group col-6">
                    <input type='text' className='form-control' id='street_number' name='street_number' placeholder='Αριθμός' 
                        value={this.state.street_number} onChange={this.handleInputChange} />
                </div>
            </div>
            <div className="form-row">
                <div className="form-group col-6">
                    <input type='text' className='form-control' id='locality' name='locality' placeholder='Δήμος' 
                        value={this.state.locality} onChange={this.handleInputChange}/>
                </div>
                <div className="form-group col-6">
                    <input type='text' className='form-control' id='postal_code' name='postal_code' placeholder='Τκ'
                        value={this.state.postal_code} onChange={this.handleInputChange} />
                </div>
            </div>
            <div className="form-group">
                <input type='text' className='form-control' id='phone' name='phone' placeholder='Τηλέφωνο' pattern="[0-9]{10}" maxLength="10" title="1234567890"
                    value={this.state.phone} onChange={this.handleInputChange}  required/>
            </div>
            <div className="form-group" style={{ margin: '0', fontSize: '12px',lineHeight: '24px'}}>
                <div className="form-check">
                <input className="" type="checkbox" id="gridCheck1" name="terms" checked={this.state.terms}
            onChange={this.handleInputChange} required/>
                <label className="form-check-label" htmlFor="gridCheck1">
                    Αποδέχομαι τους <Link to="/terms">όρους χρήσης της υπηρεσίας</Link>
                </label>
                </div>
                <div className="form-check">
                <input className="" type="checkbox" id="gridCheck2" name="newsletter"       checked={this.state.newsletter}
            onChange={this.handleInputChange} />
                <label className="form-check-label" htmlFor="gridCheck2">
                    Επιθυμώ να λαμβάνω νέα και προσφορές.
                </label>
                </div>
            </div>
            <button type="submit" className="btn btn-findfix btn-block" 
            style={{margin : '20px 0', float : 'right'}}>Αποστολή</button>

            </form>
        </div>
        )
 

        if (this.state.loading) {
           element = (
                <div style={{textAlign : 'center'}}>
                    <Gears />
                    <h4 style={{marginTop : "100px", lineHeight : "1.6"}}>
                    Ο τεχνικός εξετάζει το αίτημα και θα απαντήσει σε λίγα λεπτα.</h4>
                    <p>Μην κλείσετε αυτό το παράθυρο μέχρι να απαντήσει</p>
                    
                </div>);}

        return (
            <div>
                { element }
            </div>
            
        )
    }
}

const mapStateToProps = state => {

    return {
        addr : state.searchReducer.address,
        ctgrFilter : state.filterReducer.category,
        brndFilter : state.filterReducer.brand,
        mdlFilter : state.filterReducer.model,
        issFilter : state.filterReducer.issue,
        serWay : state.filterReducer.serviceWay,
        usrId : state.authReducer.userId,
        shpId : state.uiReducer.shopId,
        cmmntFilter : state.filterReducer.comment,
        usrData : state.userReducer.user,
        usrAdrsData : state.userReducer.address
    };
}

const mapDispatchToProps = dispatch => {
    return {
        userData : (data) => dispatch(user(data)),
        userAddrData : (data) => dispatch(address(data)),
        mShwUsr:(value, id, user) => dispatch(modalShowUser(value, id, user)),
        mShw:(value) => dispatch(modalShow(value)),
        mHide : () => dispatch(modalBackdrop()),
        emailConf : () => dispatch(issueEmailConfirmation()),
    };
};


export default connect(mapStateToProps, mapDispatchToProps)(withRouter(UserDataForLive));
