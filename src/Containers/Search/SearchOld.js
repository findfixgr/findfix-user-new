import React, { Component } from 'react';
import { connect } from 'react-redux';
// import axios from '../../axios';
import { withRouter } from 'react-router-dom'

import { mainChange, findLoc } from '../../Store/actions';
import './SearchOld.css';

class Search extends Component {


   state = { 
       loading : false
   }

   onClickHandler = () => 
   {
       this.setState({loading : true})
       this.props.onFindLocation();
   }

    render() {

        let buttonElement = <button className="locate" type="button" id="button-addon2" onClick={this.onClickHandler} >Αυτόματη εύρεση τοποθεσίας</button>


        if(this.state.loading) { buttonElement=<button className="locate" type="button" id="button-addon2" style={{fontSize : '24px'}}><i className="fa fa-circle-o-notch fa-spin"></i></button>    }

        //σβήνω ότι έχει αποθηκέυσει
        sessionStorage.clear();
        
        return(
            <div className="search">
                <div className="form-row">
                    <div className="form-group col-md-9 col-s-9 col-xs-12">
                        <input type="text" id="address" className="form-control search-form-control" onChange={this.props.onMainChange} 
                           placeholder="Περιοχή..." />
                    </div>
                    <div className="form-group col-md-3 col-s-3 col-xs-12">
                        {buttonElement}
                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        addr : state.searchReducer.address,
    };
};

const mapDispatchToProps = dispatch => {

    return {
        onMainChange: (event) => {
            event.preventDefault()
            dispatch(mainChange(event.target.value));
        },
        onFindLocation : () => dispatch(findLoc())
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(Search));