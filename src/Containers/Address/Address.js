import React, { Component } from 'react';
import { connect } from 'react-redux';
// import axios from '../../axios';
import { withRouter } from 'react-router-dom'

import { inChange, refreshChange } from '../../Store/actions';
// import styles from './Address.module.css';

class Address extends Component {
 
    clickHandler = () => {
        console.log('clicl');
        this.props.rfrshChange(localStorage.getItem('locality'));
    }

    render() {
        let placeholder = 'Περιοχή..';
        if (localStorage.getItem('locality')) {
            placeholder = localStorage.getItem('locality')};
        

        return(
            <div className="input-group mb-2" style={{width : '400px'}}>
                <div className="input-group-prepend">
                    <div className="input-group-text" onClick={this.clickHandler}><ion-icon name="search"></ion-icon></div>
                </div>
                    <input type="text" id="address" className="form-control" onChange={this.props.onInputChange}  placeholder={placeholder} />
            </div>
          
        );
    }
}

const mapStateToProps = state => {
    return {
        addr : state.searchReducer.address,
    };
};

const mapDispatchToProps = dispatch => {

    return {
        onInputChange: (event) => dispatch(inChange(event.target.value)),
        rfrshChange: (val) => dispatch(refreshChange(val)),
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(Address));