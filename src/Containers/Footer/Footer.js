import React, { Component } from 'react'

import './Footer.css';

  class Footer extends Component {
    render() {
      return (
        <div className="footer">
          <div className="footer-logo">
            <img src="../img/logo-white.svg" alt="FindFix" width="100px" />
            <div className="social-icons">
              <a href="https://www.facebook.com/FindFix.gr"><ion-icon ios="logo-facebook" md="logo-facebook"></ion-icon></a>
              <a href="https://www.twitter.com/FindFix.gr"><ion-icon ios="logo-twitter" md="logo-twitter"></ion-icon></a>
              <a href="https://www.instagram.com/FindFix.gr"><ion-icon ios="logo-instagram" md="logo-instagram"></ion-icon></a>
            </div>
          </div>
          <p style={{color : 'white'}}>2018 © made with ♥ by FindFix Team</p>
        </div>
        
        )
    }
}
export default Footer;