import React, { Component } from 'react';
import { connect } from 'react-redux';
import {categoryFilter , brandFilter, issueFilter, modelFilter, searchResult } from '../../Store/actions';
import Select2 from 'react-select2-wrapper';
import './Filters.css';
import 'react-select2-wrapper/css/select2.css';

import { Category } from '../../Config/Category';
import { Brands } from '../../Config/Brands';
import { Models } from '../../Config/Models';
import { Issues } from '../../Config/Issues';

class Filters extends Component {


  render () {
   let style = null;

// create brand filter
let brandFilterData = Brands[this.props.ctgrFilter];
if (brandFilterData) { 
    style ={color : '#fff', backgroundColor : 'red'}
  }


//create model filter

let modelFilterData = Models[this.props.brndFilter];
if (modelFilterData) { 
  
}

//create issue filter
let issueFilterData = Issues[this.props.ctgrFilter];
if ( issueFilterData && this.props.brndFilter !==null) { 
 
}

    return (
      <div>
        <div className="form-row">
          <div className="form-group col-md-3">
            <label htmlFor="category">Τι θέλεις να επισκευάσεις;</label>
            <Select2
              value ={this.props.ctgrFilter}
              data={Category}
              onChange={this.props.onCategoryFilter}
              className="form-control" 
              id="category"
        /> 
          </div>
          <div className="form-group col-md-3">
            <label htmlFor="brand">Μάρκα συσκευής;</label>
            <Select2
              value ={this.props.brndFilter}
              data={brandFilterData}
              onChange={this.props.onBrandFilter}
              className="form-control" 
              id="brand"
              style={style}
          /> 
          </div>
          <div className="form-group col-md-3" >
            <label htmlFor="issue">Πρόλημα</label>
            <Select2
              value ={this.props.issFilter}
              data={issueFilterData}
              onChange={this.props.onIssueFilter}
              className="form-control" 
              id="issue"
          /> 
          </div>

        </div>
      </div>
    );
}}


const mapStateToProps = state => {
  return {
    ctgrFilter : state.filterReducer.category,
    brndFilter : state.filterReducer.brand,
    mdlFilter : state.filterReducer.model,
    issFilter : state.filterReducer.issue,

  }; 
};

const mapDispatchToProps = dispatch => {

  return {
    onCategoryFilter: (event) => dispatch(categoryFilter(event.target.value)),
    onBrandFilter: (event) => dispatch(brandFilter(event.target.value)),
    onModelFilter: (event) => dispatch(modelFilter(event.target.value)),
    onIssueFilter: (event) => dispatch(issueFilter(event.target.value)),



  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Filters);
