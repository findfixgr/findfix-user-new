import React, { Component } from 'react';
import StarRatingComponent from 'react-star-rating-component';

import styles from './Rating.module.css';
import axios from '../../axios';
// import Loader from '../../Components/UI/Loader/Gears';


class Rating extends Component {

    state = {
          rating: 1,
          comment: '',
          shop : ''
        };
 
     componentDidMount () {
        axios.get(`/shop/${this.props.match.params.shopId}`)
        .then(response => {
            this.setState({shop : response.data});
        })
        .catch()
     }
    
      onStarClick = (nextValue, prevValue, name) => {
        this.setState({rating: nextValue});
      }

      handleChange = (event) => {
        this.setState({comment: event.target.value});
      }

      onButtonHundler = () => {
          const data ={
              comment : this.state.comment,
              stars : this.state.rating,
              userId : localStorage.getItem('userId'),
              shopId : this.state.shop._id
          }
          
          axios.post(`/user/comment/${this.props.match.params.shopId}/${localStorage.getItem('userId')}`, data)
          .then(response => {
              alert(response.data.message);
              this.props.history.replace('/');
          })
          .catch(error => {
            alert(error.response.data.message);
          })
      }
    
      render() {
        // console.log(this.props);
        const { rating, shop } = this.state;
        
        return (           
            <div className={styles.main}>
                <div className="container">
                    <h1>Αξιολόγησε το {shop.shop_name}</h1>
                    <hr />
                        <h2>Βαθμολογία</h2>
                        <p>Κάνε κλικ στα αστέρια για να διαλέξεις</p>
                        <div style={{fontSize : '48px', padding : '30px 0'}}>
                            <StarRatingComponent 
                            name="rate1" 
                            starCount={5}
                            value={rating}
                            onStarClick={this.onStarClick}
                            />
                        </div>
                    <hr />
                    <div style={{padding : '30px 0'}}>
                        <h2>Επιπλέον σχόλια</h2>
                        <textarea className="form-control"  rows="5" value={this.state.comment} onChange={this.handleChange} placeholder="Αν επιθυμείς, γράψε κάποιο σχόλιο"></textarea>
                    </div>
                    <button type="button" className="btn btn-findfix" style={{float: 'right'}} onClick={this.onButtonHundler}>Καταχώρηση</button> 
                </div>
            </div>
        );
      }
    }
    

export default Rating;