import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
import axios from '../../axios';
import { modalBackdrop, modalShowUser, favPartnerHandler } from '../../Store/actions';
import styles from './SingleshopByName.module.css';
import Loader from '../../Components/UI/Loader/Gears';
import SingleShopMap from  '../SingleShopMap/SIngleShopMap';
import Filters from '../Filters/FiltersInShop';
import ServicesFilter from '../Filters/ServicesFilters';


class Singleshop extends PureComponent {
    state = {
        result : '',
        descriptions :'',
        comments : '',
        loading : true,
        error : false
    }
    componentDidMount () {
        axios.get(`/shop/name/${this.props.match.params.locality}/${this.props.match.params.name}`)
        .then (response => {
            // console.log(response.data);
            // if(response.data === null) {
            //     this.setState({error : true});
            // }
            this.setState({result : response.data});
            
            axios.get('/partner/descriptions/'+response.data.partner._id)
            .then (descriptions => {
                this.setState({descriptions : descriptions.data,
                                loading : false});
                // console.log(descriptions);
                axios.get(`/shop/comment/${response.data._id}`)
                .then(comments => {
                    // console.log(comments.data);
                    this.setState({'comments':comments.data.docs});
                })
                .catch()
            })
            .catch (error => {
                console.log(error);
            });
        })
        .catch (error => {
            console.log(error);
        });

        
    }

    render() {
         // eslint-disable-next-line
        let color = 'antiquewhite';

        let comments = 'Δεν υπάρχουν ακόμα κριτικές για το κατάστημα';
        let descriptionServices, descriptionCategory, descriptionServicesFilter = null;
        let websiteButton = null;
        let serviceButton = <button type="button" 
                                className="btn btn btn-findfix" 
                                onClick={(value, id, user) => this.props.mShwUsr('MESSAGEADDRESS', this.state.result._id, localStorage.getItem('userId'))} 
                                >
                                <span className="flaticon-mail"></span>
                                Αίτημα επισκευής
                            </button>

        //αν εχει site και εχει γινει faved
        if (this.state.result) {
            if (this.state.result.favedBy && this.state.result.favedBy.includes(localStorage.getItem('userId'))) {
                color = 'red'
            }
            if(this.state.result.website !== '') {
                websiteButton =  <a href={`https://${this.state.result.website}`}><button type="button" className="btn btn-outline-dark"><span className="flaticon-internet"></span>website</button></a>
            }
        }

        if (this.props.faved) { color = 'red'}


        if(this.state.result.active) {
            if(this.props.iss !== null) {
            serviceButton = <button type="button" 
                                className="btn btn btn-findfix" 
                                onClick={(value, id, user) => this.props.mShwUsr('SERVICE', this.state.result._id, localStorage.getItem('userId'))} 
                                >
                                <span className="flaticon-wrench"></span>
                                Επισκευή
                            </button>
        } else {
            serviceButton = <button type="button" 
            className="btn btn btn-findfix" 
            onClick={(value, id, user) => this.props.mShwUsr('LIVESERVICE', this.state.result._id, localStorage.getItem('userId'))} 
            >
            <span className="flaticon-wrench"></span>
            Επισκευή
        </button>
        }
    }
    
        if (this.state.descriptions) {
        //map τροπους εξυπηρετησης σε <P>
        let categoryElement = null;

        descriptionServices = this.state.descriptions.services.map(value => {return (<li key={value}>{value}</li>);});
        descriptionServicesFilter = <ServicesFilter descriptionServicesData={this.state.descriptions.services} />
        descriptionCategory = this.state.descriptions.category.map(value => {
             switch (value) {
                 case 'Laptop':
                    categoryElement =<span>Laptop <i className="flaticon-laptop"></i></span>                      
                     break;
                case 'Tablet':
                    categoryElement = <span>Tablet  <i className="flaticon-tablet-1"></i></span>                     
                    break;
                case 'Σταθερός Η/Υ':
                    categoryElement = <span>Σταθερούς Η/Υ  <i className="flaticon-pc"></i></span>                     
                    break;
                case 'Κινητό':
                    categoryElement = <span>Κινητά  <i className="flaticon-smartphone"></i></span>                     
                    break;
                case 'Εκτυπωτής':
                    categoryElement = <span>Εκτυπωτές  <i className="flaticon-print"></i></span>                     
                    break;
                case 'Game Console':
                    categoryElement = <span>Παιχνιδομηχανές<i className="flaticon-console"></i></span>                     
                    break;
                 default:
                     break;
             }
             return (<p key={value}>{categoryElement}</p>);});
        }

        if (this.state.comments !== '') {
            comments = (this.state.comments).map(data => {
                let pComment = null;
                if (data.partnerComment) {
                    pComment = (<div className={styles.partnerComment}>
                                    <h6>Απάντηση τεχνικού</h6>
                                    {data.partnerComment}
                                </div>);
                }
                if (data.confirmed) {
                    return(
                    <div key={data._id}>
                        <div>{data.user.first_name} 
                            <div className={styles.starratingscss}>
                            {/* πάει το data.stars επι το font size */}
                            <div className={styles.starratingscsstop} style={{width : data.stars*13}}><span>★</span><span>★</span><span>★</span><span>★</span><span>★</span></div>
                            <div className={styles.starratingscssbottom}><span>★</span><span>★</span><span>★</span><span>★</span><span>★</span></div>
                            </div>
                        </div>
                    <div>{data.userComment}</div>
                    <div>{pComment}</div>
                    <hr />
                </div> );
                } else {
                    return null;
                }
            })
        }

        // get logo 
        let img_alt = 'Logo';
        let img_src= '/img/logo_grey.jpg';
        if (this.state.result.logo !== undefined) { 
            img_alt = this.state.result.shop_name;
        //    img_src = `https://boiling-stream-41133.herokuapp.com/${this.state.result.logo}`;
           img_src = `https://api.findfix.gr:4000/${this.state.result.logo}`; 

        }; 


        let element = <Loader />

        if (!this.state.loading) {
            element = 
            (<div className={styles.singleShop}>
                <div className={styles.singleShopMap}>
                    <SingleShopMap markers = {this.state.result.location} current={this.state.result.location} />
                </div>
                <div className="container">
                    <div className={`row`}>
                        <div className={`col-xs-12 col-sm-12 col-md-6 col-lg-3 col-xl-3 ${styles.logo}`}>
                            <img src={img_src} alt={img_alt} />
                        </div>
                        <div className={`col-xs-12 col-sm-12 col-md-6 col-lg-3 col-xl-3 ${styles.info}`}>
                        <div className={styles.title}>
                            <h1 style={{color : '#EF4657', position : 'relative'}}>{this.state.result.shop_name}
                                {/* <span onClick={() => this.props.fvdPartner(this.state.result._id)} className="flaticon-heart" style={{float : 'none', padding : '0 60px', color : color}}></span>*/}
                            </h1> 
                            </div>
                            <div>
                                <div className={styles.infoDetails}>    
                                    <p><strong><span className="flaticon-shopper"></span>Διεύθυνση</strong></p>
                                    <p>{this.state.result.route} {this.state.result.street_number}, {this.state.result.locality}, {this.state.result.postal_code}</p>
                                </div>
                                <div className={styles.infoDetails}>    
                                <p><strong><span className="flaticon-call"></span>Τηλέφωνο</strong></p>
                                <p>{this.state.result.phone_number}</p>
                                </div>
                                { websiteButton }
                            </div>
                        </div>
                        <div className={`col-xs-12 col-sm-12 col-md-12 offset-lg-2 col-lg-4 offset-xl-2 col-xl-4`}>
                        {/* <div class="position-absolute"> */}
                        <div className={styles.action}>
                        <div className="accordion" id="accordionExample">
                            <div>
                                <div id="headingOne">
                                <div>
                                    <button className={styles.showMoreButton} type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                    <span className="flaticon-wrench"></span> 
                                    Ποιο είναι το πρόβλημα
                                    <ion-icon name="arrow-dropdown"></ion-icon>
                                    </button>
                                </div>
                                </div>
                                <hr />
                                <div id="collapseOne" className="collapse hide" aria-labelledby="headingOne" data-parent="#accordionExample">
                                    <div className="card-body">
                                    <Filters />                           
                                    </div>
                                </div>
                                <div id="headingTwo">
                                <div>
                                    <button className={styles.showMoreButton} type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
                                    <span className="flaticon-help"></span> 
                                    Πως θέλεις να εξυπηρετηθείς
                                    <ion-icon name="arrow-dropdown"></ion-icon>
                                    </button>
                                </div>
                                </div>
                                <hr />
                                <div id="collapseTwo" className="collapse hide" aria-labelledby="headingTwo" data-parent="#accordionExample">
                                    <div className="card-body">
                                   { descriptionServicesFilter }                         
                                    </div>
                                </div>
                            </div>
                         
                            
                        </div>
                        { serviceButton }
                        </div>
                        </div>
                    </div>

                    <div className={styles.category}>
                            <h4>Τι επισκευάζουμε</h4>
                            <div>{descriptionCategory}</div>
                            <hr />
                            <h4>Τρόποι εξυπηρετησης</h4>
                            <div>{descriptionServices}</div>
                        </div>
                    <div className="clearfix"></div>
                    <div className={styles.comments}>
                        <h4>Αξιολογήσεις πελατών</h4>
                        <div>{ comments }</div>
                    </div>
                    <div className={styles.description}>
                        <h4>Περιγραφή</h4>
                        <div>{this.state.descriptions.details}</div>
                    </div>
                </div>
            </div>);
        }

        if (this.state.error) { element = <div>Κάτι πήγε λάθος</div>}
        return(
            <div style={{minHeight : '80vh'}}>
                <Helmet>
                    <title>{`${this.state.result.shop_name} στην περιοχή ${this.state.result.locality} | FindFix`}</title>
                    <meta name="description" content={`${this.state.result.shop_name} στην περιοχή ${this.state.result.locality}`} />
                </Helmet>
                {element}
            </div>);
    }
}

const mapStateToProps = state => {
    return {
        shp : state.shopsReducer.shops,
        addr : state.searchReducer.address,
        show : state.uiReducer.show,
        faved : state.generalReducer.fav,
        iss : state.filterReducer.issue
    };
};

const mapDispatchToProps = dispatch => {
    
    return {
        mShwUsr:(value, id, user) => dispatch(modalShowUser(value, id, user)),
        fvdPartner : (value) => dispatch(favPartnerHandler(value)),
        mHide:() => dispatch(modalBackdrop())
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Singleshop);